/***************************************************************************************************************
    This file is part of Library for 8051.

    Library for 8051 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Library for 8051 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Library for 8051. If not, see <http://www.gnu.org/licenses/>.
/**************************************************************************************************************/


/***
**   File       : delay.h
**   Author     : Sriharsha
**   Website    : www.zuna.in
**   Email      : helpzuna@gmail.com
**   Description: This is the delay routine include file for 8051 family MCU's (Count is for 11.0592 MHz)
***/


#ifndef __delay_h__
#define __delay_h__

#define DELAY_US_LOOP_VAL 1
#define DELAY_MS_LOOP_VAL 141

extern void _delay_ms(unsigned int);
extern void _delay_us(unsigned int);

#endif